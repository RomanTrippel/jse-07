package ru.trippel.tm.command.user;

import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.view.KeyboardView;

public final class UserEditCommand extends AbstractCommand {

    public UserEditCommand(final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getNameCommand() {
        return "USER_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Edit a profile.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final User user = serviceLocator.getUserService().findOne(userId);
        System.out.println("Enter new Login.");
        final String newLogin = KeyboardView.read();
        user.setLogin(newLogin);
        serviceLocator.getUserService().merge(user);
        ((Bootstrap)serviceLocator).setCurrentUser(null);
        System.out.println("Changes applied. Login required again.");
    }

}
