package ru.trippel.tm.repository;

import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final String userId) {
        final LinkedList<Project> listTemp = new LinkedList<>();
        final LinkedList<Project> listAll  = new LinkedList<>(map.values());
        for (final Project project: listAll) {
            if (project.getUserId().equals(userId)) listTemp.add(project);
        }
        return listTemp;
    }

    @Override
    public void clear(final String userId) {
        final LinkedList<Project> listAll  = new LinkedList<>(map.values());
        for (final Project project: listAll) {
            if (project.getUserId().equals(userId)) map.remove(project.getId());
        }
    }

}
