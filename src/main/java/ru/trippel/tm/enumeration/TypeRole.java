package ru.trippel.tm.enumeration;

public enum  TypeRole {

    ADMIN("Admin"),
    USER("User");

    private String displayName = "";

    TypeRole(String displayName) {
        this.displayName = displayName;
    }

    final public String getDisplayName() {
        return displayName;
    }

}
