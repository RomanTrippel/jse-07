package ru.trippel.tm.service;

import ru.trippel.tm.api.service.IService;
import ru.trippel.tm.entity.AbstractEntity;
import ru.trippel.tm.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService <T extends AbstractEntity> implements IService<T> {

    protected AbstractRepository<T> abstractRepository;

    public AbstractService(final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public List<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    public T findOne(final String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return abstractRepository.findOne(id);
    }

    @Override
    public T persist(final T t) {
        if (t == null);
        return abstractRepository.persist(t);
    }

    @Override
    public T merge(final T t) {
        if (t == null);
        return abstractRepository.merge(t);
    }

    @Override
    public T remove(final String id) {
        if (id == null) return null;
        if (id.isEmpty()) return null;
        return abstractRepository.remove(id);
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }
}
