package ru.trippel.tm.command.user;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.service.UserService;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.view.KeyboardView;

public final class UserCreateCommand extends AbstractCommand {

    public UserCreateCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "USER_CREATE";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Create a user, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        final User user = new User();
        System.out.println("Enter login name.");
        final String name = KeyboardView.read();
        if (((UserService)(serviceLocator.getUserService())).checkLogin(name)) {
            System.out.println("This name already exists, try again.");
            return;
        }
        user.setLogin(name);
        System.out.println("Enter password.");
        final String password = KeyboardView.read();
        final String passwordHash = HashPasswordUtil.getHash(password);
        user.setPassword(passwordHash);
        serviceLocator.getUserService().persist(user);
        System.out.println("User \"" + user.getLogin() + "\" added!");
    }

}
