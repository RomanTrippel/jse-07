package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class ProjectViewTasksCommand extends AbstractCommand {

    public ProjectViewTasksCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_VIEWTASK";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "View all attached tasks.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        final String projectId = projectList.get(projectNum).getId();
        for (int i = 0; i < taskList.size() ; i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                System.out.println(taskList.get(i).getName() + " - " + taskList.get(i).getDescription());
            }
        }
    }

}
