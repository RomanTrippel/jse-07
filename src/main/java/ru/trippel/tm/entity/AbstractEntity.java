package ru.trippel.tm.entity;

import java.util.UUID;

public abstract class AbstractEntity     {

    protected String id;

    public AbstractEntity() {
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

}
