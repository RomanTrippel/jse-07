package ru.trippel.tm.command.system;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "HELP";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "List commands.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Command list:");
        for (final AbstractCommand command :
                ((Bootstrap)serviceLocator).getCommands()) {
            System.out.println(command.getNameCommand() + ": "
                    + command.getDescription());
        }
    }

}