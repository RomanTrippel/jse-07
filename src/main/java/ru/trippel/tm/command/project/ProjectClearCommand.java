package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.view.KeyboardView;

import java.util.List;

public final class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_CLEAR";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        ((ProjectService)serviceLocator.getProjectService()).clear(userId);
        System.out.println("The list of projects has been cleared.");
    }

}
