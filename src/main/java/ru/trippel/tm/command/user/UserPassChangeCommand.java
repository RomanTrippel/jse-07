package ru.trippel.tm.command.user;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.view.KeyboardView;

public final class UserPassChangeCommand extends AbstractCommand {

    public UserPassChangeCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "USER_PASSCHANGE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Change Password.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final User user = serviceLocator.getUserService().findOne(userId);
        System.out.println("Enter new Password");
        final String newPassword = KeyboardView.read();
        final String newPasswordHash = HashPasswordUtil.getHash(newPassword);
        user.setPassword(newPasswordHash);
        ((Bootstrap)serviceLocator).setCurrentUser(null);
        System.out.println("Changes applied. Login required again.");
    }

}
