package ru.trippel.tm.command.task;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.view.KeyboardView;

import java.util.List;

public final class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "TASK_CLEAR";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        ((TaskService)serviceLocator.getTaskService()).clear(userId);
        System.out.println("The list of tasks has been cleared.");
    }

}
