package ru.trippel.tm.api.service;

import ru.trippel.tm.entity.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findOne(String id);

    User persist(User user);

    User merge(User user);

    User remove(String id);

}
