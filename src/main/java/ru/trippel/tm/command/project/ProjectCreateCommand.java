package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.view.KeyboardView;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_CREATE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        final Project project = new Project();
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        System.out.println("Enter project name.");
        final String name = KeyboardView.read();
        project.setName(name);
        project.setUserId(userId);
        serviceLocator.getProjectService().persist(project);
        System.out.println("The project \"" + project.getName() + "\" added!");
    }

}
