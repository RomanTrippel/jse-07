package ru.trippel.tm.command.task;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "TASK_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Edit a task.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i + 1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Edit:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        final String taskId = taskList.get(taskNum).getId();
        System.out.println("Enter new Name:");
        final String newName = KeyboardView.read();
        final Task task = serviceLocator.getTaskService().findOne(taskId);
        task.setName(newName);
        System.out.println("Enter description:");
        final String newDescription = KeyboardView.read();
        task.setDescription(newDescription);
        serviceLocator.getTaskService().merge(task);
        System.out.println("Changes applied.");
    }

}
