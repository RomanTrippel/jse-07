package ru.trippel.tm.service;

import ru.trippel.tm.api.repository.IProjectRepository;
import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.repository.AbstractRepository;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.view.KeyboardView;

import java.io.IOException;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(final AbstractRepository<Project> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    public List<Project> findAll(final String userId) {
        return abstractRepository.findAll(userId);
    }

    public void removeAllByProjectId(final String projectId) {
        if (projectId == null) return;
        if (projectId.isEmpty()) return;
        ((ITaskRepository)abstractRepository).removeAllByProjectId(projectId);
    }

    public void clear(final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        ((IProjectRepository)abstractRepository).clear(userId);
    }

}
