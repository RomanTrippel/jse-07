package ru.trippel.tm.service;

import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.repository.AbstractRepository;
import ru.trippel.tm.repository.UserRepository;

public final class UserService extends AbstractService <User> implements IUserService {

    public UserService(final AbstractRepository<User> abstractRepository) {
        super(abstractRepository);
    }

    public boolean checkLogin(final String name) {
        if (name == null) return false;
        if (name.isEmpty()) return false;
        return ((UserRepository)abstractRepository).checkLogin(name);
    }

}
