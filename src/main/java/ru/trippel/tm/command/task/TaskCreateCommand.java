package ru.trippel.tm.command.task;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "TASK_CREATE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        final Task task = new Task();
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        System.out.println("Enter task name.");
        final String name = KeyboardView.read();
        task.setName(name);
        task.setUserId(userId);
        serviceLocator.getTaskService().persist(task);
        System.out.println("The task \"" + task.getName() + "\" added!");
    }

}
