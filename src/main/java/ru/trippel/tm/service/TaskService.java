package ru.trippel.tm.service;

import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.repository.AbstractRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.view.KeyboardView;

import java.io.IOException;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(final AbstractRepository<Task> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    public List<Task> findAll(final String userId) {
        return abstractRepository.findAll(userId);
    }
    public void clear(final String userId) {
        if (userId == null) return;
        if (userId.isEmpty()) return;
        ((ITaskRepository)abstractRepository).clear(userId);
    }

}
