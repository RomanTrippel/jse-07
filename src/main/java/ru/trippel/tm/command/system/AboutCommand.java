package ru.trippel.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    public AboutCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "ABOUT";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Information about the application.";
    }

    @Override
    public void execute() {
        final String buildNumber = Manifests.read("buildNum");

        System.out.println("Information about the application:\n" +
                "build number: " + "\n" + buildNumber) ;

    }

}