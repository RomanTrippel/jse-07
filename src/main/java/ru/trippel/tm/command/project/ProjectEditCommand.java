package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_EDIT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Edit a project.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Edit:");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        final String projectId = projectList.get(projectNum).getId();
        System.out.println("Enter new Name:");
        final String newName = KeyboardView.read();
        final Project project = serviceLocator.getProjectService().findOne(projectId);
        project.setName(newName);
        System.out.println("Enter description:");
        final String newDescription = KeyboardView.read();
        project.setDescription(newDescription);
        serviceLocator.getProjectService().merge(project);
        System.out.println("Changes applied.");
    }

}
