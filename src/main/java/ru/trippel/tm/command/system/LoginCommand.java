package ru.trippel.tm.command.system;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class LoginCommand extends AbstractCommand {

    public LoginCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "LOGIN";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public String getDescription() {
        return "Authorization, you must enter a username and password.";
    }

    @Override
    public void execute() throws Exception {
        final List<User> userList = serviceLocator.getUserService().findAll();
        final User userTemp = new User();
        System.out.println("Enter login:");
        final String login = KeyboardView.read();
        userTemp.setLogin(login);
        System.out.println("Enter password:");
        final String password = KeyboardView.read();
        final String passwordHash = HashPasswordUtil.getHash(password);
        userTemp.setPassword(passwordHash);
        for (int i = 0; i <userList.size(); i++) {
            if (userList.get(i).equals(userTemp)) {
                ((Bootstrap)serviceLocator).setCurrentUser(userList.get(i));
                System.out.println("You have successfully logged in.");
                return;
            }
        }
        if (((Bootstrap)serviceLocator).getCurrentUser() == null) {
            System.out.println("You entered the login or password incorrectly.");
        }
    }

}
