package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class ProjectAttachTaskCommand extends AbstractCommand {

    public ProjectAttachTaskCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_ATTACHTASK";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Attach Task to Project.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        int projectNum = -1;
        int taskNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        final String projectId = projectList.get(projectNum).getId();
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getProjectId().isEmpty()) {
                System.out.println(i+1 + ". " + taskList.get(i).getName());
            }
        }
        System.out.println("Enter a task number");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        final String taskId = taskList.get(taskNum).getId();
        final Task task = serviceLocator.getTaskService().findOne(taskId);
        task.setProjectId(projectId);
        System.out.println("The task attached.");
    }

}
