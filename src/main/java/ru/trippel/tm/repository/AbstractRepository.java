package ru.trippel.tm.repository;

import ru.trippel.tm.api.repository.IRepository;
import ru.trippel.tm.entity.AbstractEntity;
import ru.trippel.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository <T extends AbstractEntity> implements IRepository<T> {

    protected Map<String, T> map = new LinkedHashMap<>();

    @Override
    public List<T> findAll() {
        return new LinkedList<>(map.values());
    }

    @Override
    public T findOne(final String id) {
        if (!map.containsKey(id)) return null;
        return map.get(id);
    }

    @Override
    public T persist(final T t) {
        final String tId = t.getId();
        if (map.containsKey(tId)) return null;
        return map.put(tId,t);
    }

    @Override
    public T merge(final T t) {
        final String tId = t.getId();
        if (map.containsKey(tId)) return null;
        return map.merge(tId, t, (oldT, newT) -> newT);
    }

    @Override
    public T remove(final String id) {
        if (!map.containsKey(id)) return null;
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}
