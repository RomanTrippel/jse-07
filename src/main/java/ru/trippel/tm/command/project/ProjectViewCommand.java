package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import java.util.List;

public final class ProjectViewCommand extends AbstractCommand {

    public ProjectViewCommand(IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_VIEW";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i));
        }
    }

}