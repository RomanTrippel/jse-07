package ru.trippel.tm.repository;

import ru.trippel.tm.api.repository.ITaskRepository;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public List<Task> findAll(final String userId) {
        final LinkedList<Task> listTemp = new LinkedList<>();
        final LinkedList<Task> listAll  = new LinkedList<Task>(map.values());
        for (final Task task: listAll) {
            if (task.getUserId().equals(userId)) listTemp.add(task);
        }
        return listTemp;
    }

    public void clear(final String userId){
        final LinkedList<Task> listAll  = new LinkedList<>(map.values());
        for (final Task task: listAll) {
            if (task.getUserId().equals(userId)) map.remove(task.getId());
        }
    }

    public void removeAllByProjectId(final String projectId) {
        final LinkedList<Task> listAll  = new LinkedList<>(map.values());
        for (final Task task: listAll) {
            if (task.getProjectId().equals(projectId)) map.remove(task.getId());
        }
    }

}
