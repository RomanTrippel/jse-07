package ru.trippel.tm.repository;

import ru.trippel.tm.api.repository.IUserRepository;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;
import java.util.LinkedList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public List<User> findAll(final String userId) {
        final LinkedList<User> listTemp = new LinkedList<>();
        final LinkedList<User> listAll  = new LinkedList<>(map.values());
        for (final User user: listAll) {
            if (user.getId().equals(userId)) listTemp.add(user);
        }
        return listTemp;
    }

    @Override
    public User persist(final User user) {
        final String userId = user.getId();
        if (map.containsKey(userId)) return null;
        return map.put(userId,user);
    }
    public void removeAll(final String id) {
        map.clear();

    }

    public boolean checkLogin(final String name){
        boolean result = false;
        final LinkedList<User> listAll  = new LinkedList<>(map.values());
        for (final User user: listAll) {
            if (user.getLogin().equals(name)) result = true;
        }
        return result;
    }

}
