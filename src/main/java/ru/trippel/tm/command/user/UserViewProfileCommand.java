package ru.trippel.tm.command.user;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;

import java.util.List;

public final class UserViewProfileCommand extends AbstractCommand {

    public UserViewProfileCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "USER_VIEWPROFILE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "View profile.";
    }

    @Override
    public void execute() {
        final String id = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final User user = serviceLocator.getUserService().findOne(id);
        System.out.println(user);
    }

}
