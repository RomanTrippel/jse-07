package ru.trippel.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashPasswordUtil {

    public static String getHash(final String in) throws NoSuchAlgorithmException {
        final String result;
        final String salt = "соль в начале" + in + "соль в конце";
        final MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.reset();
        digest.update(salt.getBytes());
        final BigInteger bigInt = new BigInteger(1, digest.digest());
        result = bigInt.toString(16);
        return result;
    }

}
