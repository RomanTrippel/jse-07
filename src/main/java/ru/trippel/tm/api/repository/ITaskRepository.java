package ru.trippel.tm.api.repository;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(String userId);

    Task findOne(String id);

    Task persist(Task task);

    Task merge(Task task);

    Task remove(String id);

    void removeAll();

    void clear(String userId);

    void removeAllByProjectId(String projectId);

}
