package ru.trippel.tm.api.service;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(String userId);

    Task findOne(String id);

    Task persist(Task task);

    Task merge(Task task);

    Task remove(String id);

}
