package ru.trippel.tm;

import ru.trippel.tm.context.Bootstrap;

public final class Application {

    public static void main(String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}