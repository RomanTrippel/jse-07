package ru.trippel.tm.command.task;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "TASK_REMOVE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Remove a task.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        int taskNum = -1;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i).getName());
        }
        System.out.println("Enter a task number to Remove:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        final String taskId = taskList.get(taskNum).getId();
        serviceLocator.getTaskService().remove(taskId);
        System.out.println("The task has been deleted.");
    }

}
