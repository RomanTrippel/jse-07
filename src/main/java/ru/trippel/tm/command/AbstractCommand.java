package ru.trippel.tm.command;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.service.UserService;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    protected TypeRole role;

    public AbstractCommand(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        role = TypeRole.USER;
    }

    public abstract String getNameCommand();

    public abstract boolean secure ();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public TypeRole getRole() {
        return role;
    }

    public void setRole(final TypeRole role) {
        this.role = role;
    }

}

