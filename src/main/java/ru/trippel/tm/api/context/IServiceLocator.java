package ru.trippel.tm.api.context;

import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.entity.User;

import java.util.List;

public interface IServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

}
