package ru.trippel.tm.command.user;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;

import java.util.List;

public final class UserViewCommand extends AbstractCommand {

    public UserViewCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
        role = TypeRole.ADMIN;

    }

    @Override
    public String getNameCommand() {
        return "USER_VIEW";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "View all user names.";
    }

    @Override
    public void execute() {
        final List<User> userList = serviceLocator.getUserService().findAll();
        userList.forEach(x -> System.out.println(x));
    }

}
