package ru.trippel.tm.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class KeyboardView {

    private final static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String read() throws IOException {
        return reader.readLine();
    }

}