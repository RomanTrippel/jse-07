package ru.trippel.tm.command.project;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.view.KeyboardView;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "PROJECT_REMOVE";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Remove a project.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        int projectNum = -1;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            System.out.println(i+1 + ". " + projectList.get(i).getName());
        }
        System.out.println("Enter a project number to Delete:");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        final String projectId = projectList.get(projectNum).getId();
        serviceLocator.getProjectService().remove(projectId);
        ((ProjectService)serviceLocator.getProjectService()).removeAllByProjectId(projectId);
        System.out.println("The project has been deleted.");
    }

}
