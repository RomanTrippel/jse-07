package ru.trippel.tm.entity;

import ru.trippel.tm.enumeration.TypeRole;

import java.util.Objects;

public final class User extends AbstractEntity {

    private String login = "";

    private String password = "";

    private String description = "";

    private TypeRole role = TypeRole.USER;

    public TypeRole getRole() {
        return role;
    }

    public void setRole(final TypeRole role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", description='" + description + '\'' +
                ", role=" + role +
                '}';
    }
    
}
