package ru.trippel.tm.api.service;

import java.util.List;

public interface IService<T> {

    List<T> findAll();

    T findOne(String id);

    T persist(T t);

    T merge(T t);

    T remove(String id);

    void removeAll();

}
