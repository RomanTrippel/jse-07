package ru.trippel.tm.api.repository;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.User;

import java.util.LinkedList;
import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User findOne(String id);

    User persist(User user);

    User merge(User user);

    User remove(String id);

    void removeAll();

}
