package ru.trippel.tm.command.task;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;
import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import java.util.List;

public final class TaskViewCommand extends AbstractCommand {

    public TaskViewCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "TASK_VIEW";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        final String userId = ((Bootstrap)serviceLocator).getCurrentUser().getId();
        final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            System.out.println(i+1 + ". " + taskList.get(i));
        }
    }

}
