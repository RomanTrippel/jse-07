# [Task Manager](https://gitlab.com/RomanTrippel/jse-07) 
![Task Manager](https://i.ibb.co/4WZffMt/taskmanager.png)

## Software:
```
* JDK 8
* Java 1.8
* Maven 4.0
* IntelliJ IDEA
* Windows 10
```

## Commands to build:
```bash
mvn clean
```
```bash
mvn install
```

## Command to run:
```bash
java -jar target/task-manager-7.0.jar
```

### Developer:
```
Roman Trippel

rtrippel84@gmail.com
```