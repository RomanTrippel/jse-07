package ru.trippel.tm.command.system;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.command.AbstractCommand;
import ru.trippel.tm.context.Bootstrap;

public final class LogoutCommand extends AbstractCommand {

    public LogoutCommand(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getNameCommand() {
        return "LOGOUT";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public String getDescription() {
        return "Sign out of account.";
    }

    @Override
    public void execute() throws Exception {
        ((Bootstrap)serviceLocator).setCurrentUser(null);
        System.out.println("You are signed out of your account.");
    }

}
