package ru.trippel.tm.api.service;

import ru.trippel.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(String userId);

    Project findOne(String id);

    Project persist(Project project);

    Project merge(Project project);

    Project remove(String id);

}
