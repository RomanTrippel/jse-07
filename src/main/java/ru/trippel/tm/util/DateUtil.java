package ru.trippel.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    private final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private static Date parseDate(final String date) throws ParseException {
        return simpleDateFormat.parse(date);
    }

}