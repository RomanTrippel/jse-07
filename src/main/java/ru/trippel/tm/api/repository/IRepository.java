package ru.trippel.tm.api.repository;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    List<T> findAll(String userId);

    T findOne(String id);

    T persist(T t);

    T merge(T t);

    T remove(String id);

    void removeAll();

//    void removeAll(String id);
}
