package ru.trippel.tm.context;

import ru.trippel.tm.api.context.IServiceLocator;
import ru.trippel.tm.api.service.IProjectService;
import ru.trippel.tm.api.service.ITaskService;
import ru.trippel.tm.api.service.IUserService;
import ru.trippel.tm.command.*;
import ru.trippel.tm.command.project.*;
import ru.trippel.tm.command.system.*;
import ru.trippel.tm.command.task.*;
import ru.trippel.tm.command.user.*;
import ru.trippel.tm.entity.User;
import ru.trippel.tm.enumeration.TypeRole;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.repository.UserRepository;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.service.UserService;
import ru.trippel.tm.util.HashPasswordUtil;
import ru.trippel.tm.view.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements IServiceLocator {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final IProjectService projectService = new ProjectService(new ProjectRepository());

    private final ITaskService taskService = new TaskService(new TaskRepository());

    private final IUserService userService = new UserService(new UserRepository());

    private  User currentUser = null;

    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void start() throws Exception {
        BootstrapView.printWelcome();
        String command = "";
        init();
        while (true) {
            command = KeyboardView.read();
            if ("EXIT".equals(command)) {
                end();
            }
            if (command.isEmpty()) {
                BootstrapView.printError();
                continue;
            }
            execute(getCommand(command));
        }
    }

    private void execute(final AbstractCommand command) {
        try {
            checkPermission(command).execute();
        } catch (Exception e) {
            BootstrapView.printError();
        }
    }

    private AbstractCommand getCommand(String command) {
        if (command == null) {
            return null;
        }
        if (command.isEmpty()) {
            return null;
        }
        final AbstractCommand abstractCommand = commands.get(command);
        return abstractCommand;
    }

    private AbstractCommand checkPermission(AbstractCommand command) {
        if (command == null) {
            return null;
        }
        if (command.secure()) return command;
        if (TypeRole.ADMIN == getCurrentUser().getRole()) {
            return command;
        }
        if (getCurrentUser().getRole() == command.getRole()) {
            return command;
        }
        return null;
    }

    public void init() throws NoSuchAlgorithmException {
        commands.put("HELP", new HelpCommand(this));
        commands.put("LOGIN", new LoginCommand(this));
        commands.put("USER_CREATE", new UserCreateCommand(this));
        commands.put("PROJECT_CREATE", new ProjectCreateCommand(this));
        commands.put("PROJECT_VIEW", new ProjectViewCommand(this));
        commands.put("PROJECT_EDIT", new ProjectEditCommand(this));
        commands.put("PROJECT_REMOVE", new ProjectRemoveCommand(this));
        commands.put("PROJECT_CLEAR", new ProjectClearCommand(this));
        commands.put("PROJECT_ATTACHTASK", new ProjectAttachTaskCommand(this));
        commands.put("PROJECT_VIEWTASK", new ProjectViewTasksCommand(this));
        commands.put("TASK_CREATE", new TaskCreateCommand(this));
        commands.put("TASK_VIEW", new TaskViewCommand(this));
        commands.put("TASK_EDIT", new TaskEditCommand(this));
        commands.put("TASK_REMOVE", new TaskRemoveCommand(this));
        commands.put("TASK_CLEAR", new TaskClearCommand(this));
        commands.put("USER_VIEWPROFILE", new UserViewProfileCommand(this));
        commands.put("USER_VIEW", new UserViewCommand(this));
        commands.put("USER_EDIT", new UserEditCommand(this));
        commands.put("USER_PASSCHANGE", new UserPassChangeCommand(this));
        commands.put("LOGOUT", new LogoutCommand(this));
        commands.put("ABOUT", new AboutCommand(this));
        commands.put("EXIT", new ExitCommand(this));
        final User admin = new User();
        admin.setLogin("ADMIN");
        admin.setPassword(HashPasswordUtil.getHash("ADMIN"));
        admin.setRole(TypeRole.ADMIN);
        userService.persist(admin);
        final User user = new User();
        user.setLogin("USER");
        user.setPassword(HashPasswordUtil.getHash("USER"));
        userService.persist(user);
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    private void end() {
        BootstrapView.printGoodbye();
        System.exit(0);
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

}
